const test = require('blue-tape')
const configuration = require('../core.conf.example.js')

const mongo = require('./helpers/database')(configuration)
require('./helpers/setup')(configuration)

test('Enforce unique index on domain', async (t) => {
  const domain = 'example.com'
  const collections = [
    'hosts',
    'configurations',
    'certificates'
  ]
  for (const collection of collections) {
    await mongo.db.collection(collection).deleteMany()
    await mongo.db.collection(collection).insertOne({domain})
    await t.shouldFail(
      mongo.db.collection(collection).insertOne({domain})
    )
  }
})
