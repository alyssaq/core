const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {fetch} = require('./helpers/fetch')

require('./helpers/setup')(configuration)

test('Deprecated v0 deploy endpoint', async (t) => {
  const url = `https://localhost:${configuration.port}/deploy`
  const headers = {'content-type': 'multipart/form-data'}
  const response = await fetch(url, {method: 'POST', headers})
  t.is(response.status, 426)
  const body = await response.text()
  t.true(/Update required/.test(body))
})

test('Deprecated v1 deploy endpoint', async (t) => {
  const url = `https://localhost:${configuration.port}/sites`
  const headers = {'content-type': 'multipart/form-data'}
  const response = await fetch(url, {method: 'POST', headers})
  t.is(response.status, 426)
  const body = await response.text()
  t.true(/Update required/.test(body))
})
