const {fetch} = require('./fetch')

const credentials = {
  user: {
    grant_type: 'password',
    audience: process.env.COMMONSHOST_CORE_JWT_AUDIENCE,
    scope: ['openid', 'deploy'].join(' '),
    client_id: process.env.COMMONSHOST_CORE_TEST_CLIENT_ID,
    connection: 'Username-Password-Authentication',
    username: process.env.COMMONSHOST_CORE_TEST_USERNAME,
    password: process.env.COMMONSHOST_CORE_TEST_PASSWORD
  },
  edge: {
    grant_type: 'client_credentials',
    audience: process.env.COMMONSHOST_EDGE_AUTH0_AUDIENCE,
    scope: ['global_read'].join(' '),
    client_id: process.env.COMMONSHOST_EDGE_AUTH0_CLIENT_ID,
    client_secret: process.env.COMMONSHOST_EDGE_AUTH0_CLIENT_SECRET
  }
}

const accessTokens = new WeakMap()

async function getAccessToken (body) {
  if (accessTokens.has(body) === false) {
    const method = 'POST'
    const endpoint = 'oauth/token'
    const headers = {}
    const issuer = process.env.COMMONSHOST_CORE_JWT_ISSUER
    const url = issuer + endpoint
    const options = {
      body: JSON.stringify(body),
      headers: Object.assign(
        {'content-type': 'application/json'},
        headers
      ),
      method
    }
    const response = await fetch(url, options)
    const data = await response.json()
    if (data['access_token'] === undefined) {
      throw new Error(`Failed to get a token: ${data['error_description']}`)
    }
    const accessToken = data['access_token']
    accessTokens.set(body, accessToken)
  }
  return accessTokens.get(body)
}

async function getUserAccessToken () {
  return getAccessToken(credentials.user)
}

async function getEdgeAccessToken () {
  return getAccessToken(credentials.edge)
}

async function getUserId () {
  const accessToken = await getUserAccessToken()
  const jwt = accessToken.split('.')[1]
  const raw = Buffer.from(jwt, 'base64').toString()
  const {sub: userId} = JSON.parse(raw)
  return userId
}

module.exports.getUserAccessToken = getUserAccessToken
module.exports.getEdgeAccessToken = getEdgeAccessToken
module.exports.getUserId = getUserId
