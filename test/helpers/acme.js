#! /usr/bin/env node

const {join} = require('path')
const {promisify} = require('util')
const {writeFile} = require('fs')
const mkdirp = require('mkdirp')

const argv = require('minimist')(process.argv.slice(2))

const domain = Array.isArray(argv.domain) ? argv.domain[0] : argv.domain
const directory = join(argv.home, `${domain}_ecc`)

if (!domain) {
  console.error('Missing argument: domain')
  process.exit(1)
}

if (!argv.home) {
  console.error('Missing argument: home')
  process.exit(1)
}

const files = [
  {
    path: join(directory, `${domain}.key`),
    data: 'private key'
  },
  {
    path: join(directory, 'fullchain.cer'),
    data: 'public certificate chain'
  }
]

async function main () {
  console.log(`Generating mock certificate for: ${domain}`)
  console.log(`Creating directory: ${directory}`)
  await promisify(mkdirp)(directory)
  for (const {path, data} of files) {
    console.log(`Creating file: ${path}`)
    await promisify(writeFile)(path, data)
  }
}

main()
