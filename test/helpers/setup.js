const test = require('blue-tape')
const server = require('../..')

let fastify

module.exports = (configuration) => {
  test('Starting server', async (t) => {
    if (fastify === undefined) {
      fastify = await server(configuration)
      await fastify.listen(configuration.port, '::')
      const halt = async () => {
        await fastify.close()
        fastify = undefined
      }
      test.onFinish(halt)
      test.onFailure(halt)
    }
  })
  return () => fastify
}
