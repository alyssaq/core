const {Agent} = require('https')
const fetch = require('node-fetch')

module.exports.fetch = (url, options = {}) => {
  if (options.agent === undefined) {
    options.agent = new Agent({
      rejectUnauthorized: false,
      ecdhCurve: 'P-384:P-256'
    })
  }
  return fetch(url, options)
}
