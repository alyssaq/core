const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {getUserAccessToken} = require('./helpers/credentials')
const {fetch} = require('./helpers/fetch')
const {toASCII} = require('punycode/')

require('./helpers/setup')(configuration)

test('Get domain name suggestions', async (t) => {
  const accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/suggestions`
  const response = await fetch(url, {
    method: 'GET',
    headers: {'authorization': `Bearer ${accessToken}`}
  })
  t.is(response.status, 200)
  const domains = await response.json()
  t.ok(Array.isArray(domains))
  t.isNot(domains.length, 0)
  t.ok(domains.length <= 4)
  for (const domain of domains) {
    t.is(typeof domain, 'string')
  }
})

test('Turn project name into suggestion', async (t) => {
  const accessToken = await getUserAccessToken()
  const given = '@some/thing'
  const expected = 'some-thing'
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/suggestions?project=${given}`
  const response = await fetch(url, {
    method: 'GET',
    headers: {'authorization': `Bearer ${accessToken}`}
  })
  t.is(response.status, 200)
  const domains = await response.json()
  t.ok(domains.length >= 1)
  t.ok(domains.find((domain) => domain.startsWith(expected)))
})

test('Support IDN', async (t) => {
  const accessToken = await getUserAccessToken()
  const given = '💩'
  const expected = toASCII(given)
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/domains/suggestions` +
    `?project=${encodeURIComponent(given)}`
  const response = await fetch(url, {
    method: 'GET',
    headers: {'authorization': `Bearer ${accessToken}`}
  })
  t.is(response.status, 200)
  const domains = await response.json()
  t.ok(domains.length >= 1)
  t.ok(domains.find((domain) => domain.includes(expected)))
})
