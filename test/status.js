const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {fetch} = require('./helpers/fetch')

require('./helpers/setup')(configuration)

test('Report status and uptime', async (t) => {
  const url = `https://localhost:${configuration.port}/`
  const response = await fetch(url)
  t.is(response.status, 200)
  const json = await response.json()
  t.is(json.status, 'OK')
  t.is(typeof json.uptime, 'number')
})
