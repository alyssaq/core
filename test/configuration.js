const test = require('blue-tape')

const {defaultOptions} = require('../configuration/defaultOptions')
const {defaultHost} = require('../configuration/defaultHost')
const {ConfigurationValidator} = require('@commonshost/configuration')
const defaultsDeep = require('lodash.defaultsdeep')

test('Validate default configuration schema', async (t) => {
  const validator = new ConfigurationValidator()
  const host = defaultsDeep({}, defaultHost)
  const options = defaultsDeep({hosts: [host]}, defaultOptions)
  host.domain = 'foo'
  t.doesNotThrow(() => validator.validate(options))
})
