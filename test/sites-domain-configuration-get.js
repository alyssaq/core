const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {
  getEdgeAccessToken,
  getUserAccessToken,
  getUserId
} = require('./helpers/credentials')
const {fetch} = require('./helpers/fetch')

const mongo = require('./helpers/database')(configuration)
require('./helpers/setup')(configuration)

const fixture = {}

test('Setup database fixtures', async (t) => {
  fixture.domain = 'example.com'
  fixture.userId = await getUserId()
  fixture.modified = new Date()
  fixture.configuration = {foo: 'bar'}

  await mongo.db.collection('users').deleteMany()
  await mongo.db.collection('users').insertMany([{
    modified: fixture.modified,
    userId: fixture.userId
  }])
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([{
    modified: fixture.modified,
    domain: fixture.domain,
    ownerId: fixture.userId
  }])
  await mongo.db.collection('configurations').deleteMany()
  await mongo.db.collection('configurations').insertMany([{
    modified: fixture.modified,
    domain: fixture.domain,
    configuration: fixture.configuration
  }])
})

test('Retrieve site configuration as user', async (t) => {
  const accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/sites/${fixture.domain}/configuration`
  const response = await fetch(url, {
    method: 'GET',
    headers: {'authorization': `Bearer ${accessToken}`}
  })
  t.true(response.ok)
  const actual = await response.json()
  const expected = {
    domain: fixture.domain,
    modified: fixture.modified.toISOString(),
    configuration: fixture.configuration
  }
  t.deepEqual(actual, expected)
})

test('Retrieve site configuration as edge', async (t) => {
  const accessToken = await getEdgeAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/sites/${fixture.domain}/configuration`
  const response = await fetch(url, {
    method: 'GET',
    headers: {'authorization': `Bearer ${accessToken}`}
  })
  t.true(response.ok)
  const actual = await response.json()
  const expected = {
    domain: fixture.domain,
    modified: fixture.modified.toISOString(),
    configuration: fixture.configuration
  }
  t.deepEqual(actual, expected)
})
