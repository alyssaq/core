const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {getUserAccessToken, getUserId} = require('./helpers/credentials')
const {fetch} = require('./helpers/fetch')
const PubNub = require('pubnub')
const FormData = require('form-data')
const shellescape = require('shell-escape')
const {exec} = require('child-process-promise')
const {join} = require('path')
const rimraf = require('rimraf')
const {promisify} = require('util')
const {chmod} = require('fs')

const acmesh = configuration.acme.acmesh
const acmejs = join(__dirname, 'helpers/acme.js')

const mongo = require('./helpers/database')(configuration)
require('./helpers/setup')(configuration)

const fixture = {}

test('Setup database fixtures', async (t) => {
  fixture.userId = await getUserId()
  fixture.modified = new Date()
  fixture.domain = 'fixture.example.net'

  await mongo.db.collection('users').deleteMany()
  await mongo.db.collection('users').insertMany([{
    modified: fixture.modified,
    userId: fixture.userId
  }])
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('configurations').deleteMany()
  await mongo.db.collection('certificates').deleteMany()
})

test('Reset & mock acme.sh', async (t) => {
  configuration.acme.acmesh = acmejs
  await promisify(chmod)(acmejs, '755')
  await promisify(rimraf)(configuration.acme.home)
})

test('Resetting object storage', async (t) => {
  try {
    await exec(shellescape([
      configuration.s3.s3cmd,
      `--access_key=${configuration.s3.accessKeyId}`,
      `--secret_key=${configuration.s3.secretAccessKey}`,
      `--region=${configuration.s3.region}`,
      `--host=${configuration.s3.endpoint}`,
      `--host-bucket=%(bucket)s.${configuration.s3.endpoint}`,
      '--recursive',
      '--force',
      'del',
      `s3://${configuration.s3.bucket}`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to reset object storage')
  }
})

test('Deploy a site', async (t) => {
  const accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const domain = fixture.domain
  const url = `${origin}/v2/sites/${domain}`

  const form = new FormData()

  form.append(
    'configuration',
    JSON.stringify({domain}),
    {contentType: 'application/json'}
  )

  const files = [
    {
      data: 'hello world',
      filepath: 'index.html'
    },
    {
      data: 'fake code',
      filepath: 'scripts/app.js'
    }
  ]

  for (const {data, filepath} of files) {
    form.append('directory', data, {filepath})
  }

  const expectedDeploy = {
    type: 'site-deploy',
    domain,
    isNewDomain: true,
    hasNewFiles: true,
    hasNewConfiguration: true
  }

  const subscribeKey = configuration.pubnub.subscribeKey
  const pubnub = new PubNub({subscribeKey})
  pubnub.subscribe({channels: configuration.pubnub.channels})
  const pubnubNotification = new Promise((resolve) => {
    const expectedMessageCount = 2
    let receivedMessageCount = 0
    pubnub.addListener({
      status (status) {
        console.log(status)
      },
      message ({message}) {
        if (message.type === 'site-deploy') {
          t.deepEqual(message, expectedDeploy)
          receivedMessageCount++
        }
        if (message.type === 'certificate-issue') {
          const expectedIssue = {
            type: 'certificate-issue',
            domain
          }
          t.deepEqual(message, expectedIssue)
          receivedMessageCount++
        }
        if (receivedMessageCount === expectedMessageCount) {
          pubnub.stop()
          resolve()
        }
      }
    })
  })

  const response = await fetch(url, {
    method: 'PUT',
    headers: {'authorization': `Bearer ${accessToken}`},
    body: form
  })

  t.true(response.ok)
  t.deepEqual(await response.json(), expectedDeploy)

  const hostOptions = await mongo.db.collection('configurations')
    .findOne({domain: fixture.domain})
  t.not(hostOptions, null)
  t.is(hostOptions.domain, domain)
  t.true('configuration' in hostOptions)
  t.true('modified' in hostOptions)

  let output

  try {
    output = await exec(shellescape([
      configuration.s3.s3cmd,
      `--access_key=${configuration.s3.accessKeyId}`,
      `--secret_key=${configuration.s3.secretAccessKey}`,
      `--region=${configuration.s3.region}`,
      `--host=${configuration.s3.endpoint}`,
      `--host-bucket=%(bucket)s.${configuration.s3.endpoint}`,
      'ls',
      `s3://${configuration.s3.bucket}/sites/${domain}/public/`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to list object storage')
  }
  t.ok(/public\/index\.html/.test(output.stdout))

  try {
    output = await exec(shellescape([
      configuration.s3.s3cmd,
      `--access_key=${configuration.s3.accessKeyId}`,
      `--secret_key=${configuration.s3.secretAccessKey}`,
      `--region=${configuration.s3.region}`,
      `--host=${configuration.s3.endpoint}`,
      `--host-bucket=%(bucket)s.${configuration.s3.endpoint}`,
      'ls',
      `s3://${configuration.s3.bucket}/sites/${domain}/crypto/`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to list object storage')
  }
  t.ok(/crypto\/key.pem/.test(output.stdout))
  t.ok(/crypto\/cert.pem/.test(output.stdout))

  await pubnubNotification
})

test('Restore acme.sh', async (t) => {
  configuration.acme.acmesh = acmesh
})
