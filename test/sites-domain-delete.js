const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {getUserAccessToken, getUserId} = require('./helpers/credentials')
const {fetch} = require('./helpers/fetch')
const PubNub = require('pubnub')
const shellescape = require('shell-escape')
const {exec} = require('child-process-promise')
const {join} = require('path')

const mongo = require('./helpers/database')(configuration)
require('./helpers/setup')(configuration)

const fixture = {}

test('Setup database fixtures', async (t) => {
  fixture.domain = 'example.com'
  fixture.assets = [
    join(__dirname, 'fixtures/index.html')
  ]
  fixture.userId = await getUserId()
  fixture.modified = new Date()
  fixture.configuration = {}
  fixture.san = []

  await mongo.db.collection('users').deleteMany()
  await mongo.db.collection('users').insertMany([{
    modified: fixture.modified,
    userId: fixture.userId
  }])
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([{
    modified: fixture.modified,
    domain: fixture.domain,
    ownerId: fixture.userId
  }])
  await mongo.db.collection('configurations').deleteMany()
  await mongo.db.collection('configurations').insertMany([{
    domain: fixture.domain,
    modified: fixture.modified,
    configuration: fixture.configuration
  }])
  await mongo.db.collection('certificates').deleteMany()
  await mongo.db.collection('certificates').insertMany([{
    domain: fixture.domain,
    san: fixture.san
  }])
})

test('Resetting object storage', async (t) => {
  try {
    await exec(shellescape([
      configuration.s3.s3cmd,
      `--access_key=${configuration.s3.accessKeyId}`,
      `--secret_key=${configuration.s3.secretAccessKey}`,
      `--region=${configuration.s3.region}`,
      `--host=${configuration.s3.endpoint}`,
      `--host-bucket=%(bucket)s.${configuration.s3.endpoint}`,
      '--recursive',
      '--force',
      'del',
      `s3://${configuration.s3.bucket}`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to reset object storage')
  }
  try {
    await exec(shellescape([
      configuration.s3.s3cmd,
      `--access_key=${configuration.s3.accessKeyId}`,
      `--secret_key=${configuration.s3.secretAccessKey}`,
      `--region=${configuration.s3.region}`,
      `--host=${configuration.s3.endpoint}`,
      `--host-bucket=%(bucket)s.${configuration.s3.endpoint}`,
      'put',
      ...fixture.assets,
      `s3://${configuration.s3.bucket}/sites/${fixture.domain}/`
    ]))
  } catch (error) {
    throw new Error(error.stderr || 'Failed to prepare object storage')
  }
})

test('Delete a site', async (t) => {
  const pubnub = new PubNub({subscribeKey: configuration.pubnub.subscribeKey})
  pubnub.subscribe({channels: configuration.pubnub.channels})
  const pubnubNotification = new Promise((resolve) => {
    pubnub.addListener({
      status (status) {
        console.log(status)
      },
      message ({message}) {
        t.deepEqual(message, {type: 'site-delete', domain: fixture.domain})
        pubnub.stop()
        resolve()
      }
    })
  })

  const accessToken = await getUserAccessToken()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/sites/${encodeURIComponent(fixture.domain)}`
  const response = await fetch(url, {
    method: 'DELETE',
    headers: {'authorization': `Bearer ${accessToken}`}
  })
  t.true(response.ok)
  for (const collection of [
    'hosts',
    'configurations',
    'certificates'
  ]) {
    const result = await mongo.db.collection(collection)
      .findOne({domain: fixture.domain})
    t.is(result, null)
  }

  try {
    const {stdout, stderr} = await exec(shellescape([
      configuration.s3.s3cmd,
      `--access_key=${configuration.s3.accessKeyId}`,
      `--secret_key=${configuration.s3.secretAccessKey}`,
      `--region=${configuration.s3.region}`,
      `--host=${configuration.s3.endpoint}`,
      `--host-bucket=%(bucket)s.${configuration.s3.endpoint}`,
      'ls',
      `s3://${configuration.s3.bucket}/sites/${fixture.domain}/`
    ]))
    t.is(stdout, '')
    t.is(stderr, '')
  } catch (error) {
    throw new Error(error.stderr || 'Failed to list object storage')
  }

  await pubnubNotification
})
