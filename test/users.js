const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const {getUserAccessToken, getUserId} = require('./helpers/credentials')
const {fetch} = require('./helpers/fetch')

const mongo = require('./helpers/database')(configuration)
require('./helpers/setup')(configuration)

test('Setup database fixtures', async (t) => {
  await mongo.db.collection('users').deleteMany()
  await mongo.db.collection('users').insertMany([{
    modified: new Date(),
    userId: await getUserId()
  }])
})

test('Change email address', async (t) => {
  const accessToken = await getUserAccessToken()
  const userId = await getUserId()
  const origin = `https://localhost:${configuration.port}`
  const url = `${origin}/v2/users/${encodeURIComponent(userId)}`
  const random = Math.floor(Math.random() * 1e10)
  const expectedEmail = `test.${random}@example.net`
  const response = await fetch(url, {
    method: 'PATCH',
    headers: {
      'content-type': 'application/json',
      'authorization': `Bearer ${accessToken}`
    },
    body: JSON.stringify({
      email: expectedEmail
    })
  })
  t.is(response.status, 200)
  const user = await response.json()
  t.is(user.email, expectedEmail)
})
