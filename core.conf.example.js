const {tmpdir, homedir} = require('os')
const {join} = require('path')
const {execSync} = require('child_process')

module.exports = {
  // Top level domain for automatically generated subdomains
  wildcard: 'commons.host',

  // Directory where uploaded files will be stored before processing
  upload: tmpdir(),

  // Directory where public files will be stored
  sites: join(process.cwd(), 'sites'),

  // Object Storage
  s3: {
    s3cmd: execSync('which s3cmd').toString().trim(),
    region: process.env.COMMONSHOST_CORE_S3_REGION,
    endpoint: process.env.COMMONSHOST_CORE_S3_ENDPOINT,
    bucket: process.env.COMMONSHOST_CORE_S3_BUCKET,
    accessKeyId: process.env.COMMONSHOST_CORE_S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.COMMONSHOST_CORE_S3_SECRET_ACCESS_KEY
  },

  // Database
  mongodb: {
    database: process.env.COMMONSHOST_CORE_MONGODB_DATABASE,
    url: process.env.COMMONSHOST_CORE_MONGODB_URL
  },

  // Data Stream
  pubnub: {
    uuid: process.env.COMMONSHOST_CORE_PUBNUB_UUID,
    channels: process.env.COMMONSHOST_CORE_PUBNUB_CHANNELS.split(' '),
    publishKey: process.env.COMMONSHOST_CORE_PUBNUB_PUBLISH_KEY,
    subscribeKey: process.env.COMMONSHOST_CORE_PUBNUB_SUBSCRIBE_KEY
  },

  // Authentication Tokens
  jwt: {
    jwksUri: process.env.COMMONSHOST_CORE_JWT_JWKSURI,
    audience: process.env.COMMONSHOST_CORE_JWT_AUDIENCE,
    issuer: process.env.COMMONSHOST_CORE_JWT_ISSUER
  },

  // Auth0 Management API
  // https://auth0.com/docs/api/management/v2/tokens#1-get-a-token
  auth0: {
    issuer: process.env.COMMONSHOST_CORE_JWT_ISSUER,
    audience: process.env.COMMONSHOST_CORE_AUTH0_AUDIENCE,
    clientId: process.env.COMMONSHOST_CORE_AUTH0_CLIENT_ID,
    clientSecret: process.env.COMMONSHOST_CORE_AUTH0_CLIENT_SECRET
  },

  // Port to accept incoming connections
  port: process.env.COMMONSHOST_CORE_PORT || 18443,

  // TLS credentials
  crypto: {
    key: join(homedir(), '.commonshost/key.pem'),
    cert: join(homedir(), '.commonshost/cert.pem'),
    ca: []
  },

  // LetsEncrypt
  acme: {
    acmesh: join(homedir(), '.acme.sh/acme.sh'),
    home: join(process.cwd(), 'acme/store'),
    webroot: join(process.cwd(), 'acme/webroot'),
    port: process.env.COMMONSHOST_CORE_ACME_PORT || 18080,
    prefix: '/.well-known/acme-challenge/'
  }
}
