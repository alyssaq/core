const shellEscape = require('shell-escape')
const {exec} = require('child-process-promise')
const {s3cmd} = require('./s3cmd')
const {join} = require('path')
const {copyFile, unlink} = require('fs')
const {promisify} = require('util')

async function issueCertificate (fastify, domain) {
  const {configuration, mongo: {db}, pubnub} = fastify
  const domains = [domain]

  const certificate = await db.collection('certificates').findOne({domain})
  if (certificate !== null && Array.isArray(certificate.san)) {
    const alternates = certificate.san
      .filter((alternate) => alternate !== domain)
    const uniques = new Set(alternates)
    domains.push(...uniques)
  }

  try {
    await exec(shellEscape([
      configuration.acme.acmesh,
      '--issue',
      '--keylength', 'ec-256',
      '--webroot', configuration.acme.webroot,
      '--home', configuration.acme.home,
      ...domains.reduce((args, name) => [...args, '--domain', name], [])
    ]))
  } catch (error) {
    if (!error.stderr.includes('Domain key exists')) {
      throw error
    }
  }

  const directory = join(configuration.acme.home, `${domain}_ecc`)

  await Promise.all([
    promisify(copyFile)(
      join(directory, `${domain}.key`),
      join(directory, 'key.pem')
    ),
    promisify(copyFile)(
      join(directory, 'fullchain.cer'),
      join(directory, 'cert.pem')
    )
  ])

  await s3cmd(
    configuration.s3,
    'put',
    join(directory, 'key.pem'),
    join(directory, 'cert.pem'),
    `s3://${configuration.s3.bucket}/sites/${domain}/crypto/`
  )

  await Promise.all([
    promisify(unlink)(join(directory, 'key.pem')),
    promisify(unlink)(join(directory, 'cert.pem'))
  ])

  await db.collection('certificates').updateOne(
    {domain},
    {
      $set: {domain},
      $currentDate: {issued: true}
    },
    {upsert: true}
  )

  pubnub.publish({
    channel: configuration.pubnub.channels[0],
    message: {type: 'certificate-issue', domain}
  })
}

module.exports.issueCertificate = issueCertificate
