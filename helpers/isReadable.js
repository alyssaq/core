const {promisify} = require('util')
const {access, constants: {R_OK}} = require('fs')

async function isReadable (filepath) {
  try {
    await promisify(access)(filepath, R_OK)
    return true
  } catch (error) {
    return false
  }
}

module.exports = isReadable
