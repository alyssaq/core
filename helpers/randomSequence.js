const {animals, adjectives} = require('human-readable-ids/lists')
const superb = require('superb')

function randomSequence () {
  const prefix = superb().replace(/[^a-z]/g, '')
  const adjective = adjectives[Math.floor(Math.random() * adjectives.length)]
  const animal = animals[Math.floor(Math.random() * animals.length)]
  return `${prefix}-${adjective}-${animal}`
}

module.exports = randomSequence
