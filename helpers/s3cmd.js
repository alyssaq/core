const {InsufficientStorage} = require('http-errors')
const shellEscape = require('shell-escape')
const {exec} = require('child-process-promise')

async function s3cmd (s3, ...args) {
  try {
    return await exec(shellEscape([
      s3.s3cmd,
      `--access_key=${s3.accessKeyId}`,
      `--secret_key=${s3.secretAccessKey}`,
      `--region=${s3.region}`,
      `--host=${s3.endpoint}`,
      `--host-bucket=%(bucket)s.${s3.endpoint}`,
      ...args
    ]))
  } catch (error) {
    const message = error.stderr || 'Failed to access object storage'
    throw new InsufficientStorage(message)
  }
}

module.exports.s3cmd = s3cmd
