const {normalize} = require('path')
const slash = require('slash')
const sanitize = require('sanitize-filename')

function fileFilter (req, file, cb) {
  const cleanpath = slash(normalize(file.originalname))

  for (const segment of cleanpath.split('/')) {
    if (segment !== sanitize(segment)) {
      return cb(null, false)
    }
  }

  if (cleanpath.startsWith('/')) {
    return cb(null, false)
  }

  if (cleanpath === '') {
    return cb(null, false)
  }

  file.originalname = cleanpath
  cb(null, true)
}

module.exports = fileFilter
