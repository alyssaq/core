# AWS IAM Policies

## Users

- commonshost-core-production
- commonshost-core-test
- commonshost-edge-production
- commonshost-edge-test

## Buckets

- commonshost-production
- commonshost-test

## Policies

### Upload or Delete Files

#### core-production

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::commonshost-production",
                "arn:aws:s3:::commonshost-production/*"
            ]
        }
    ]
}
```

#### core-test

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::commonshost-test",
                "arn:aws:s3:::commonshost-test/*"
            ]
        }
    ]
}
```

### List and Read Files

#### edge-production

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::commonshost-production",
                "arn:aws:s3:::commonshost-production/*"
            ]
        }
    ]
}
```

#### edge-test

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::commonshost-test",
                "arn:aws:s3:::commonshost-test/*"
            ]
        }
    ]
}
```
