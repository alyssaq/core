module.exports.modifyUser = function () {
  return async function (request, reply) {
    const userId = request.req.user.sub
    const {db} = this.mongo
    await db.collection('users').update(
      {userId},
      {
        $set: {userId},
        $currentDate: {modified: true}
      },
      {upsert: true}
    )
  }
}
