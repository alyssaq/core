const {toASCII} = require('punycode/')

module.exports = async (request, reply) => {
  for (const data of [request.query, request.params]) {
    if (data && ('domain' in data) && (typeof data.domain === 'string')) {
      const unsafe = data.domain
      const safe = toASCII(unsafe).toLowerCase()
      data.domain = safe
    }
  }
}
