const createError = require('http-errors')
const fetch = require('node-fetch')

module.exports = async (fastify, options) => {
  let expiration = 0
  let token

  async function getManagementToken () {
    if (Date.now() < expiration) {
      return token
    }

    const url = options.issuer + 'oauth/token'
    const response = await fetch(url, {
      method: 'POST',
      headers: {'content-type': 'application/json'},
      body: JSON.stringify({
        grant_type: 'client_credentials',
        client_id: options.clientId,
        client_secret: options.clientSecret,
        audience: options.audience
      })
    })
    if (response.ok === false) {
      throw createError(response.status, response.statusText)
    }
    const data = await response.json()
    token = data.access_token
    expiration = Date.now() + data.expires_in
    return token
  }

  fastify.decorate('auth0Token', getManagementToken)
}

module.exports[Symbol.for('skip-override')] = true
