module.exports.defaultHost = {
  domain: '',
  root: '',
  directories: {
    trailingSlash: 'always'
  },
  fallback: {},
  cacheControl: {
    immutable: []
  },
  accessControl: {
    allowOrigin: '*'
  },
  serviceWorker: {
    allowed: '/'
  },
  strictTransportSecurity: {
    maxAge: 60 * 24 * 3600
  },
  manifest: []
}
