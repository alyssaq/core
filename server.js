const {readFileSync} = require('fs')
const {MongoClient} = require('mongodb')

module.exports = async (configuration) => {
  if (configuration === undefined) {
    throw new Error('Missing server configuration')
  }

  const fastify = require('fastify')({
    logger: true,
    http2: true,
    https: {
      allowHTTP1: true,
      ecdhCurve: 'P-384:P-256',
      key: readFileSync(configuration.crypto.key),
      cert: readFileSync(configuration.crypto.cert),
      ca: configuration.crypto.ca.map((filepath) => readFileSync(filepath))
    }
  })

  {
    const client = await MongoClient.connect(configuration.mongodb.url)
    const db = client.db(configuration.mongodb.database)
    const collections = ['hosts', 'configurations', 'certificates']
    await Promise.all(collections.map((collection) => {
      return db.collection(collection).createIndex('domain', {unique: true})
    }))
    await client.close()
  }

  fastify.decorate('configuration', configuration)
  fastify.addHook('preHandler', require('./hooks/normaliseDomain'))
  fastify.register(require('./plugins/jwt'), configuration.jwt)
  fastify.register(require('./plugins/auth0'), configuration.auth0)
  fastify.register(require('fastify-mongodb'), configuration.mongodb)
  fastify.register(require('./plugins/pubnub'), configuration.pubnub)
  fastify.register(require('./plugins/acme'), configuration.acme)
  fastify.use(require('cors')())
  fastify.register(require('./routes/get'))
  fastify.register(require('./routes/deprecated'))

  for (const route of [
    require('./routes/configurations/get'),
    require('./routes/domains/available/get'),
    require('./routes/domains/suggestions/get'),
    // require('./routes/sites/domain/certificate/get'),
    // require('./routes/sites/domain/certificate/put'),
    require('./routes/sites/domain/configuration/get'),
    // require('./routes/sites/domain/configuration/patch'),
    // require('./routes/sites/domain/configuration/put'),
    // require('./routes/sites/domain/files/directory/get'),
    // require('./routes/sites/domain/files/file/delete'),
    // require('./routes/sites/domain/files/file/put'),
    // require('./routes/sites/domain/info/get'),
    // require('./routes/sites/domain/info/patch'),
    // require('./routes/sites/domain/info/put'),
    require('./routes/sites/domain/delete'),
    require('./routes/sites/domain/put'),
    require('./routes/sites/get'),
    require('./routes/users/id/patch')
  ]) {
    fastify.register(route, {prefix: 'v2'})
  }

  return fastify
}
