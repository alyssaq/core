async function updateNotice (request, reply) {
  reply.code(426).send('🚧 Update required: npm i @commonshost/cli@latest')
}

module.exports = async (fastify, options) => {
  fastify.addContentTypeParser(
    'multipart/form-data',
    (request, done) => done(null, request)
  )

  fastify.post('/deploy', updateNotice)
  fastify.post('/sites', updateNotice)
}
