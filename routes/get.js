module.exports = async (fastify, options) => {
  fastify.route({
    method: 'GET',
    url: '/',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            status: {type: 'string'},
            uptime: {type: 'number'}
          }
        }
      }
    },
    handler: async (request, reply) => {
      reply.send({
        status: 'OK',
        uptime: Math.floor(process.uptime())
      })
    }
  })
}
