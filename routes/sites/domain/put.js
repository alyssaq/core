const {dirname, join} = require('path')
const {promisify} = require('util')
const {createReadStream, createWriteStream} = require('fs')
const {rename} = require('graceful-fs')
const mkdirp = require('mkdirp')
const tmp = require('tmp-promise')
const {modifyUser} = require('../../../hooks/modifyUser')
const mime = require('mime/lite')
const compressible = require('compressible')
const {compressStream: brotli} = require('iltorb')
const {createGzip: gzip} = require('node-zopfli')
const streamToPromise = require('stream-to-promise')
const {s3cmd} = require('../../../helpers/s3cmd')
const jwtPermissions = require('express-jwt-permissions')
const multer = require('multer')
const bytes = require('bytes')
const {ConfigurationValidator} = require('@commonshost/configuration')
const {BadRequest, Unauthorized, Forbidden} = require('http-errors')
const isValidDomain = require('is-valid-domain')
const isDomainName = require('is-domain-name')
const {lowestCommonAncestor} = require('lowest-common-ancestor')
const {fromAfter} = require('from-after')
const {issueCertificate} = require('../../../helpers/issueCertificate')
const fileFilter = require('../../../helpers/fileFilter')

module.exports = async (fastify, options) => {
  fastify.addContentTypeParser(
    'multipart/form-data',
    (request, done) => done(null, request)
  )

  fastify.use(
    jwtPermissions({permissionsProperty: 'scope'})
      .check('deploy')
  )

  fastify.use(multer({
    dest: fastify.configuration.upload,
    preservePath: true,
    fileFilter,
    limits: {
      fileSize: bytes('100MB'),
      files: 100000
    }
  }).array('directory'))

  fastify.route({
    method: 'PUT',
    url: '/sites/:domain',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            type: {type: 'string', value: 'site-deploy'},
            domain: {type: 'string'},
            isNewDomain: {type: 'boolean'},
            hasNewFiles: {type: 'boolean'},
            hasNewConfiguration: {type: 'boolean'}
          }
        }
      }
    },
    beforeHandler: [
      modifyUser()
    ],
    handler: async (request, reply) => {
      const {configuration} = fastify
      const {db} = fastify.mongo
      const {domain} = request.params
      const userId = request.req.user.sub
      let hostConfiguration
      let hasNewConfiguration = false

      if (!isValidDomain(domain) || !isDomainName(domain)) {
        throw new BadRequest('Invalid domain name')
      }

      if (request.req.body && request.req.body.configuration) {
        const total = request.req.body.configuration.length
        const limit = 1e4
        if (total > limit) {
          throw new Forbidden(
            `Configuration (${total} bytes) exceeds limit (${limit} bytes).`
          )
        }
        hostConfiguration = JSON.parse(request.req.body.configuration)
        const validator = new ConfigurationValidator()
        try {
          validator.validate({hosts: [hostConfiguration]})
        } catch (error) {
          throw new BadRequest(
            'Invalid configuration: Does not comply with the JSON Schema.'
          )
        }
        delete hostConfiguration.domain
        delete hostConfiguration.root
        hasNewConfiguration = true
      }

      const hasNewFiles = Array.isArray(request.req.files) &&
        request.req.files.length > 0

      if (hasNewFiles === true) {
        const limit = 1e6
        const total = request.req.files.reduce((sum, size) => sum + size, 0)
        if (total > limit) {
          throw new Forbidden(
            `Files (${total} bytes) exceed limit (${limit} bytes).`
          )
        }
      }

      let isNewDomain = false
      try {
        const result = await db.collection('hosts').updateOne(
          {domain, ownerId: userId},
          {
            $set: {domain, ownerId: userId},
            $currentDate: {modified: true}
          },
          {upsert: true}
        )
        isNewDomain = result.modifiedCount === 0
      } catch (error) {
        request.log.error(error)
        throw new Unauthorized(`Unauthorized domain: "${domain}"`)
      }

      if (hasNewConfiguration) {
        await db.collection('configurations').updateOne(
          {domain},
          {
            $set: {domain, configuration: hostConfiguration},
            $currentDate: {modified: true}
          },
          {upsert: true}
        )
      } else {
        try {
          await db.collection('configurations').insertOne({
            domain,
            configuration: {},
            modified: new Date()
          })
        } catch (error) {
        }
      }

      const stagingDirectory = await tmp.dir({unsafeCleanup: true})

      if (isNewDomain === true) {
        try {
          await db.collection('certificates').insertOne({domain, san: []})
        } catch (error) {
        }
        try {
          issueCertificate(fastify, domain)
        } catch (error) {
          request.log.error(error)
        }
      }

      if (hasNewFiles === true) {
        const originalnames = new Set(
          request.req.files.map(({originalname}) => originalname)
        )
        const ancestor = lowestCommonAncestor(...originalnames)

        for (const file of request.req.files) {
          const filepath = fromAfter.call(file.originalname, ancestor)
          const destination = join(stagingDirectory.path, 'public', filepath)
          await promisify(mkdirp)(dirname(destination))
          await promisify(rename)(file.path, destination)
          if (compressible(mime.getType(destination))) {
            const encodings = [['gz', gzip], ['br', brotli]]
            for (const [extension, encoder] of encodings) {
              if (!originalnames.has(`${file.originalname}.${extension}`)) {
                const output = createWriteStream(`${destination}.${extension}`)
                createReadStream(destination).pipe(encoder()).pipe(output)
                await streamToPromise(output)
              }
            }
          }
        }

        await s3cmd(
          configuration.s3,
          '--delete-removed',
          'sync',
          join(stagingDirectory.path, 'public') + '/',
          `s3://${configuration.s3.bucket}/sites/${domain}/public/`
        )
      }

      stagingDirectory.cleanup()

      const response = {
        type: 'site-deploy',
        domain,
        isNewDomain,
        hasNewFiles,
        hasNewConfiguration
      }

      await fastify.pubnub.publish({
        message: response,
        channel: configuration.pubnub.channels[0]
      })

      reply.send(response)
      request.log.info(`Deployed domain: ${domain}`)
    }
  })
}
