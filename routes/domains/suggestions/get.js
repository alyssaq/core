const {toASCII} = require('punycode/')
const jwtPermissions = require('express-jwt-permissions')
const {modifyUser} = require('../../../hooks/modifyUser')
const isDomainName = require('is-domain-name')
const randomSequence = require('../../../helpers/randomSequence')

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({permissionsProperty: 'scope'})
      .check('deploy')
  )

  fastify.route({
    method: 'GET',
    url: '/domains/suggestions',
    schema: {
      querystring: {
        project: {type: 'string'}
      },
      response: {
        200: {
          type: 'array',
          items: {type: 'string'}
        }
      }
    },
    beforeHandler: [
      modifyUser()
    ],
    handler: async (request, reply) => {
      const {configuration} = fastify
      const {db} = fastify.mongo

      const suggestions = [
        `${randomSequence()}.${configuration.wildcard}`,
        `${randomSequence()}.${configuration.wildcard}`,
        `${randomSequence()}.${configuration.wildcard}`,
        `${randomSequence()}.${configuration.wildcard}`
      ]

      if (request.query.project) {
        const project = toASCII(
          request.query.project
            .toLowerCase()
            .replace(/[.//@$#%&*^():[\]{}|\\]+/g, '-')
            .replace(/^-+/, '')
            .replace(/-*$/, '')
        )
        if (isDomainName(project)) {
          suggestions.unshift(
            `${project}.${configuration.wildcard}`
          )
        }
      }

      const squatted = (await db.collection('hosts').find(
        {domain: {$in: suggestions}},
        {domain: 1}
      ).toArray()).map(({domain}) => domain)

      reply.send(
        suggestions
          .filter((domain) => !squatted.includes(domain))
          .slice(0, 4)
      )
    }
  })
}
