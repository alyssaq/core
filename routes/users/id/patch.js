const createError = require('http-errors')
const {patch} = require('request')
const tls = require('tls')
const {constants} = require('http2')
const jwtPermissions = require('express-jwt-permissions')

const headerBlacklist = [
  constants.HTTP2_HEADER_CONNECTION,
  constants.HTTP2_HEADER_UPGRADE,
  constants.HTTP2_HEADER_HOST,
  constants.HTTP2_HEADER_HTTP2_SETTINGS,
  constants.HTTP2_HEADER_KEEP_ALIVE,
  constants.HTTP2_HEADER_PROXY_CONNECTION,
  constants.HTTP2_HEADER_TRANSFER_ENCODING
]

tls.DEFAULT_ECDH_CURVE = 'P-384:P-256'

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({permissionsProperty: 'scope'})
      .check('deploy')
  )

  fastify.route({
    method: 'PATCH',
    url: '/users/:id',
    handler: async (request, reply) => {
      if (request.params.id !== request.req.user.sub) {
        const FORBIDDEN = 403
        reply.send(createError(
          FORBIDDEN,
          'User to be acted on does not match subject in bearer token.'
        ))
        return
      }

      const id = encodeURIComponent(request.params.id)
      const url = `${fastify.configuration.auth0.audience}users/${id}`

      const body = {}
      if ('email' in request.body) body.email = request.body.email
      if ('password' in request.body) body.password = request.body.password

      const options = {
        body: JSON.stringify(body),
        headers: {
          'content-type': 'application/json',
          authorization: `Bearer ${await fastify.auth0Token()}`
        }
      }

      patch(url, options)
        .on('error', (error) => {
          reply.send(error)
        })
        .on('response', (response) => {
          reply.code(response.statusCode)
          for (const key of Object.keys(response.headers)) {
            if (!headerBlacklist.includes(key)) {
              reply.header(key, response.headers[key])
            }
          }
          reply.send(response)
        })
    }
  })
}
